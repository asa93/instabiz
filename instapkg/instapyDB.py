import psycopg2
import datetime
db_user = "postgres"
db_pass = "mdp"
db1 = "db1"
class BotAction:
    """Contains Everything About An Action in order to store it in DB"""
    i = 12345
    
    BOT_FOLLOW_PERSON = "bot follow person"
    PERSON_FOLLOW_BOT= "person follow bot"
    BOT_UNFOLLOW_PERSON = "bot unfollow person"
    PERSON_UNFOLLOW_BOT = "person unfollow bot"
    
    def __init__(self, timestamp, actionType, objectName="", account="", routineName = ""):
        self.timestamp = timestamp
        self.actionType = actionType
        self.objectUsername = objectName
        self.routineName = routineName
        self.account = account
        
    def __str__(self):
         return str(self.timestamp) + " : " + str(self.actionType) + " action - " + str(self.objectUsername) + " object username - "
        
def printActions(actions):
    res = []
    for action in actions:
        print(str(action))
        
    
def DBInsertAction(action):
    conn = psycopg2.connect("dbname="+db1+" user="+db_user+" password="+db_pass+"")
    cur = conn.cursor()

    cur.execute("INSERT INTO actions (date, action_type, object, account,routine_name) VALUES (%s, %s,%s,%s,%s)",(action.timestamp,action.actionType, action.objectUsername,action.account,action.routineName))

    conn.commit()
    cur.close()
    conn.close()
    
def DBInsertActions(actions):
    conn = psycopg2.connect("dbname="+db1+" user="+db_user+" password="+db_pass+"")
    cur = conn.cursor()
    for action in actions:
        cur.execute("INSERT INTO actions (date, action_type, object, account,routine_name) VALUES (%s, %s,%s,%s,%s)",(action.timestamp,action.actionType, action.objectUsername,action.account,action.routineName))

    conn.commit()
    cur.close()
    conn.close()
    
def DBInsertRoutine(routine):
    conn = psycopg2.connect("dbname="+db1+" user="+db_user+" password="+db_pass+"")
    cur = conn.cursor()

    cur.execute("INSERT INTO routine_log (routine_name, routine_start_time, routine_end_time, account,error,actions_number) VALUES (%s, %s,%s,%s,%s)",(routine.__name__,routine.startTime, routine.endTime,routine.account.username,len(routine.actions)))

    conn.commit()
    cur.close()
    conn.close()

def DBGetFollowBack():
    usernames=[]
    conn = psycopg2.connect("dbname="+db1+" user="+db_user+" password="+db_pass+"")
    cur = conn.cursor()
    cur.execute("SELECT object FROM actions Where action_type = '"+BotAction.PERSON_FOLLOW_BOT+"'")
    
    for r in cur:
        usernames.append(r[0])
        
    conn.commit()
    cur.close()
    conn.close()
    return usernames

def getRoutineActions(routine_name):
    usernames=[]
    conn = psycopg2.connect("dbname="+db1+" user="+db_user+" password="+db_pass+"")
    cur = conn.cursor()
    cur.execute("SELECT object FROM actions Where routine_name = '"+  routine_name +"'")
    
    for r in cur:
        usernames.append(r[0])
    conn.commit()
    cur.close()
    conn.close()
    return usernames

def DBGetUnfollow():
    #list of person to unfollow
    usernames=[]
    conn = psycopg2.connect("dbname="+db1+" user="+db_user+" password="+db_pass+"")
    cur = conn.cursor()
    cur.execute("SELECT object FROM actions Where (action_type = '"+BotAction.BOT_FOLLOW_PERSON+"' AND object NOT IN (select object FROM actions where action_type = '"+BotAction.PERSON_FOLLOW_BOT+"' ) AND  extract(day from localtimestamp-date) >=0) OR action_type = '"+BotAction.PERSON_UNFOLLOW_BOT+"' ") 
    
    for r in cur:
        usernames.append(r[0])
    conn.commit()
    cur.close()
    conn.close()
    return usernames

def DBUnfollowedBy():
    #list of person that unfollowed the bot
    usernames=[]
    conn = psycopg2.connect("dbname="+db1+" user="+db_user+" password="+db_pass+"")
    cur = conn.cursor()
    cur.execute("SELECT object FROM actions Where action_type = '"+BotAction.PERSON_UNFOLLOW_BOT+"'")
    
    for r in cur:
        usernames.append(r[0])
    conn.commit()
    cur.close()
    conn.close()
    return usernames

